package test.model

import spray.json.DefaultJsonProtocol._
import spray.json._

object Model {

  final case class Item(id: String, householdId: String, data: String, identified: String)

  implicit val itemFormat: RootJsonFormat[Item] = jsonFormat4(Item)

  def toJson(items: List[Item]): String = items.toJson.toString()
}