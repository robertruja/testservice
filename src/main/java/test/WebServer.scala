package test

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.{path, _}
import akka.http.scaladsl.server.Route
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import akka.http.scaladsl.model.StatusCodes
import test.model.Model
import test.model.Model.Item

import scala.concurrent.{ExecutionContextExecutor, Future}
import scala.io.StdIn

object WebServer {

  val config: Config = ConfigFactory.load
  implicit val system: ActorSystem = ActorSystem("household-system", config)
  implicit val materializer: ActorMaterializer = ActorMaterializer()
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val logger = Logging(system, getClass)

  def main(args: Array[String]) {
    val routes: Route =
      path("items") {
        get {
          parameters('item1.as[String]) { householdId =>
            println(s"Get item: $householdId")
            val getResult: Future[List[Item]] = Future {
              List(Item("1","","",""),
                Item("2","","",""),
               Item("2","","",""))
            }

            onSuccess(getResult) {
              case result if result.nonEmpty => complete(Model.toJson(result))
              case _ => complete(StatusCodes.NotFound)
            }
          }
        }
      }

    Http().bindAndHandle(routes, "0.0.0.0", 8080)

    logger.info(s"Household Service is online!\n")
    StdIn.readLine()
  }
}