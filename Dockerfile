FROM openjdk:8-jre
MAINTAINER ENDAVA

ENV SSH_PASSWD "root:Docker!"
RUN apt-get update \
        && apt-get install -y --no-install-recommends dialog \
        && apt-get update \
  && apt-get install -y --no-install-recommends openssh-server \
  && echo "$SSH_PASSWD" | chpasswd

ENTRYPOINT ["/usr/bin/java", "-jar", "/householdservice/householdservice.jar"]

ARG JAR_FILE
ARG PORT
ADD target/${JAR_FILE} /householdservice/householdservice.jar
EXPOSE ${PORT}